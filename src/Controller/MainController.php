<?php

namespace App\Controller;

use App\Entity\Image;
use App\Repository\ImageRepository;
use App\Repository\VideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class HomePageController. Main
 * @package App\Controller
 */
class MainController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @Template()
     * @param ImageRepository $ir
     * @return array
     */
    public function index(ImageRepository $ir): array
    {
        return [
            'mainPhoto' => $ir->getMainPhotoImg(),
            'mainVideo' => $ir->getMainVideoImg()
        ];
    }

    /**
     * @Route("/video", name="video")
     * @Template()
     * @param VideoRepository $vr
     * @return array
     */
    public function video(VideoRepository $vr): array
    {
        return [
            'videos' => $vr->findAll(),
        ];
    }

    /**
     * @Route("/categories", name="cat_list")
     * @Template()
     * @param ImageRepository $ir
     * @return array
     */
    public function categories(ImageRepository $ir): array
    {
        return[
            'familyPhoto' => $ir->getCatFamilyImg(),
            'fashionPhoto' => $ir->getCatFashionImg(),
            'weddingPhoto' => $ir->getCatWeddingImg()
        ];
    }

    /**
     * @Route("/category/{cat}", name="images")
     * @Template()
     * @param string $cat
     * @param ImageRepository $ir
     * @return array
     */
    public function images(string $cat, ImageRepository $ir): array
    {
        $cat = ucfirst($cat);
        $types = array_flip(Image::getTypes());
        if (!isset($types[$cat])) {
            throw new NotFoundHttpException('page not found');
        }
        $idCat = $types[$cat];
        return [
            'images' => $ir->findBy(['type' => $idCat],['id' => 'DESC']),
            'cat' => $cat
        ];
    }
}