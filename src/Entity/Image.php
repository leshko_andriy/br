<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="images")
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 * @Vich\Uploadable
 */
class Image
{
    public const TYPE_WEDDING = 1;
    public const TYPE_FASHION = 2;
    public const TYPE_FAMILY = 3;

    private const TYPES = [
        self::TYPE_WEDDING => 'Wedding',
        self::TYPE_FAMILY => 'Family',
        self::TYPE_FASHION => 'Fashion',
    ];
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="smallint")
     * @var integer
     */
    private $type;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $mainTitlePhoto = 0;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $mainTitleVideo = 0;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $catTitleWedding = 0;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $catTitleFashion = 0;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $catTitleFamily = 0;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $image;

    /**
     * @Assert\NotBlank()
     * @Assert\File(
     *     maxSize = "4096k",
     *     mimeTypes = {"image/jpeg", "image/png"},
     *     mimeTypesMessage = "Please upload a valid Image"
     * )
     * @Vich\UploadableField(mapping="images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     * @var \DateTime
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime('now', new \DateTimeZone('UTC'));
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string $image|null
     */
    public function setImage(?string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return File|null
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @param File $imageFile
     * @throws \Exception
     */
    public function setImageFile(File $imageFile): void
    {
        $this->imageFile = $imageFile;

        if ($imageFile) {
            $this->updatedAt = new \DateTime('now', new \DateTimeZone('UTC'));
        }
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return array
     */
    public static function getTypes(): array
    {
        return self::TYPES;
    }

    /**
     * @return integer
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param integer $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return self::getTypes()[$this->type];
    }

    /**
     * @return bool
     */
    public function isMainTitlePhoto(): bool
    {
        return $this->mainTitlePhoto;
    }

    /**
     * @param bool $mainTitlePhoto
     */
    public function setMainTitlePhoto(bool $mainTitlePhoto): void
    {
        $this->mainTitlePhoto = $mainTitlePhoto;
    }

    /**
     * @return bool
     */
    public function isMainTitleVideo(): bool
    {
        return $this->mainTitleVideo;
    }

    /**
     * @param bool $mainTitleVideo
     */
    public function setMainTitleVideo(bool $mainTitleVideo): void
    {
        $this->mainTitleVideo = $mainTitleVideo;
    }

    /**
     * @return bool
     */
    public function isCatTitleWedding(): bool
    {
        return $this->catTitleWedding;
    }

    /**
     * @param bool $catTitleWedding
     */
    public function setCatTitleWedding(bool $catTitleWedding): void
    {
        $this->catTitleWedding = $catTitleWedding;
    }

    /**
     * @return bool
     */
    public function isCatTitleFashion(): bool
    {
        return $this->catTitleFashion;
    }

    /**
     * @param bool $catTitleFashion
     */
    public function setCatTitleFashion(bool $catTitleFashion): void
    {
        $this->catTitleFashion = $catTitleFashion;
    }

    /**
     * @return bool
     */
    public function isCatTitleFamily(): bool
    {
        return $this->catTitleFamily;
    }

    /**
     * @param bool $catTitleFamily
     */
    public function setCatTitleFamily(bool $catTitleFamily): void
    {
        $this->catTitleFamily = $catTitleFamily;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

}
