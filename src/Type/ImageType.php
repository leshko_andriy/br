<?php

namespace App\Type;

use App\Entity\Image;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ImageType. form type image type
 * @package App\Type
 */
class ImageType extends ChoiceType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(
            [
                'required' => true,
                'choices' => array_flip(Image::getTypes()),
            ]
        );
    }
}