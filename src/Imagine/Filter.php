<?php

namespace App\Imagine;

use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\Point;
use Liip\ImagineBundle\Imagine\Filter\Loader\LoaderInterface;

/**
 * Class MyCustomFilter. Filter for gallery
 * @package AppBundle\Imagine
 */
class Filter implements LoaderInterface
{
    /**
     * @param ImageInterface $image
     * @param array          $options
     *
     * @return ImageInterface
     */
    public function load(ImageInterface $image, array $options = []): ImageInterface
    {
        $width = $image->getSize()->getWidth();
        $height = $image->getSize()->getHeight();

        $widthMustBe = $options[0];
        $heightMustBe = (int)$height * ($widthMustBe / $width);

        if ($heightMustBe < $options[1]) {
            $heightMustBe = $options[1];
        }

        $image->resize(new Box($widthMustBe, $heightMustBe));

        $y = 0;
        if ($heightMustBe > $options[1]) {
            $y = ($heightMustBe / 2) - 300;
        }

        $image->crop(new Point(0, $y), new Box($options[0], $options[1]));

        return $image;
    }
}