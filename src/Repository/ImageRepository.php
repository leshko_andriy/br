<?php

namespace App\Repository;

use App\Entity\Image;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * reposirory for images class
 * @method Image|null find($id, $lockMode = null, $lockVersion = null)
 * @method Image|null findOneBy(array $criteria, array $orderBy = null)
 * @method Image[]    findAll()
 * @method Image[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImageRepository extends ServiceEntityRepository
{
    /**
     * ImageRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Image::class);
    }

    /**
     * @return Image|null
     */
    public function getMainVideoImg(): ?Image
    {
        $items = $this->createQueryBuilder('i')
            ->where('i.mainTitleVideo = :bool')
            ->setParameter('bool',true)
            ->getQuery()
            ->getResult();

        return $this->getOne($items);
    }

    /**
     * @return Image|null
     */
    public function getMainPhotoImg(): ?Image
    {
        $items = $this->createQueryBuilder('i')
            ->where('i.mainTitlePhoto = :bool')
            ->setParameter('bool',true)
            ->getQuery()
            ->getResult();

        return $this->getOne($items);
    }

    /**
     * @return Image|null
     */
    public function getCatWeddingImg(): ?Image
    {
        $items = $this->createQueryBuilder('i')
            ->where('i.catTitleWedding = :bool')
            ->andWhere('i.type = :type')
            ->setParameters(['bool' => true, 'type' => Image::TYPE_WEDDING])
            ->getQuery()
            ->getResult();

        return $this->getOne($items);
    }

    /**
     * @return Image|null
     */
    public function getCatFamilyImg(): ?Image
    {
        $items = $this->createQueryBuilder('i')
            ->where('i.catTitleFamily = :bool')
            ->andWhere('i.type = :type')
            ->setParameters(['bool' => true, 'type' => Image::TYPE_FAMILY])
            ->getQuery()
            ->getResult();

        return $this->getOne($items);
    }

    /**
     * @return Image|null
     */
    public function getCatFashionImg(): ?Image
    {
        $items = $this->createQueryBuilder('i')
            ->where('i.catTitleFashion = :bool')
            ->andWhere('i.type = :type')
            ->setParameters(['bool' => true, 'type' => Image::TYPE_FASHION])
            ->getQuery()
            ->getResult();

        return $this->getOne($items);
    }

    /**
     * @param array $items
     * @return Image|null
     */
    private function getOne(array $items): ?Image
    {
        if (count($items) === 0) {
            $items = $this->findAll();
            if (count($items) === 0) {
                return null;
            }
        }

        shuffle($items);
        return $items[0];
    }
}
